﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    public Text timerText;
    private float startTime;

    // Use this for initialization
    void Start () {
        startTime = Time.time;
	}

    private void OnEnable()
    {
        startTime = Time.time;
    }

    // Update is called once per frame
    void Update() {
        if (GameManager.instance.gameState == GameManager.State.playing)
        {
            float elapsed = Time.time - startTime;
           
            timerText.text = ConvertToTimeToText(elapsed);
            GameManager.instance.rawScore = elapsed ;
        }
    }

    public static string ConvertToTimeToText(float elapsed)
    {
        string minutes = ((int)elapsed / 60).ToString();
        string seconds = ((int)elapsed % 60).ToString();
        if ((int)elapsed % 60 < 10)
            return minutes + ":0" + seconds;
        else
            return minutes + ":" + seconds;
    }
}
