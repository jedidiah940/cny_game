﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBoard : MonoBehaviour {

    public static ScoreBoard instance;
    public InputField input;
    public bool isInputConfirmed = false, submit;
    public Text[] highscore_names = new Text[6];
    public Text[] highscore_scores = new Text[6];
    public GameObject inputHelpText;

    public class Score{
        public string name;
        public float score;

        public Score(string _name, float _score)
        {
            name = _name;
            score = _score;
        }
    }

    List<Score> scoreList = new List<Score>();

    private void OnEnable()
    {
        submit = false;
        isInputConfirmed = false;
        input.text = "";
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        isInputConfirmed = false;

        for (int i = 0; i < highscore_names.Length; i++)
        {
            highscore_names[i].text = "";
            highscore_scores[i].text = "";
            //scoreList.Add(new Score("", 0));
        }
    }


    private void Update()
    {
        bool update = false;
        if (input.text != "" && input.text != null)
        {
            inputHelpText.SetActive(true);
        }
        else
        {
            inputHelpText.SetActive(false);
        }

        if (submit)
        {
            input.text = "";
        }

        if (Input.GetButtonDown("Submit") && !submit && input.text != null)
        {
            isInputConfirmed = true;
            update = true;
            submit = true;
        }

        if (isInputConfirmed && update)
        {
            input.DeactivateInputField();
            AddScore(input.text, GameManager.instance.rawScore);
            input.text = "";
            UpdateScoreBoard();
            update = false;
        }
    }

    public void AddScore(string player, float score)
    {
        Score newScore = new Score(player, score);
        scoreList.Add(newScore);
    }

    void SaveScoreBoard()
    {

    }

    void LoadScoreBoard()
    {

    }

    void ClearScoreBoard()
    {

    }

    void UpdateScoreBoard()
    {
        scoreList.Sort((x, y) => x.score.CompareTo(y.score));
        for (int i = 0; i < highscore_names.Length; i++)
        {
            if (scoreList[i] != null)
            {
                highscore_names[i].text = scoreList[i].name;
                if (scoreList[i].score == 0)
                {
                    highscore_scores[i].text = "";
                }
                else
                {
                    highscore_scores[i].text = Timer.ConvertToTimeToText(scoreList[i].score);
                }
            }
        }
    }
}
