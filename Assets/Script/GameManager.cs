﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public GameObject[] pictures;
    public GameObject[] words;
    public Transform[] picturePositions;
    public Transform[] wordPositions;
    public int matchedCardNo;
    public enum State { playing, win };
    public State gameState;
    public Canvas gameScreen, winScreen;
    public float rawScore;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        gameState = State.playing;
    }

    // Use this for initialization
    void Start()
    {
        gameState = State.playing;
        Card.MatchedCard += AddCard;
        GameSetup();
    }

    // Update is called once per frame
    void Update()
    {
        if (matchedCardNo == pictures.Length)
        {
            gameState = State.win;
        }

        switch (gameState)
        {
            case State.playing:
                winScreen.gameObject.SetActive(false);
                gameScreen.gameObject.SetActive(true);
                break;
            case State.win:
                winScreen.gameObject.SetActive(true);
                gameScreen.gameObject.SetActive(false);
                break;
            default:
                break;
        }
    }

    void GameSetup()
    {
        matchedCardNo = 0;
        //randomise word and picture positions
        RandomPlace(pictures, picturePositions);
        RandomPlace(words, wordPositions);
    }

    void RandomPlace(GameObject[] cards, Transform[] positions)
    {
        List<Transform> positionList = new List<Transform>();
        positionList.AddRange(positions);
        foreach (GameObject card in cards)
        {
            int random = Random.Range(0, positionList.Count);
            Instantiate(card, positionList[random]);
            positionList.Remove(positionList[random]);
        }
    }

    void AddCard()
    {
        matchedCardNo++;
        Debug.Log(matchedCardNo);
    }

    public void StartGame()
    {
        Debug.Log("Start Game");
        gameState = State.playing;
        GameSetup();
        foreach (Transform position in picturePositions)
        {
            Destroy(position.GetChild(0).gameObject);
        }
        foreach (Transform position in wordPositions)
        {
            Destroy(position.GetChild(0).gameObject);
        }
    }
}
