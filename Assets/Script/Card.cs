﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Card : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    public bool isPicture = true;
    public string cardName;
    public static float smoothing = 5;
    public Sprite back;
    private Sprite face;

    public GameObject explosionPrefab;
    public Text feedbackText;
    public Transform aboveLayer;
    public Transform originalParent;

    bool flipped = false, dragged = false, matched = false, returning = false, home = true;
    Card otherCard = null;
    Vector3 originalPosition;
    Image sprite;

    public delegate void MatchCard();
    public static event MatchCard MatchedCard;

    private void Awake()
    {
        originalPosition = transform.position;
        sprite = GetComponent<Image>();
        face = GetComponent<Image>().sprite;
        aboveLayer = GameObject.Find("above").GetComponent<Transform>();
        originalParent = transform.parent;
        feedbackText = GameObject.Find("feedback").GetComponent<Text>();
        feedbackText.text = "";

        if (!isPicture)
        {
            sprite.sprite = back;
            flipped = false;
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        StopCoroutine("ReturnToOriginalPosition");
        dragged = true;
        returning = false;
        home = false;
        if (!matched && !isPicture)
        {
            transform.SetParent(aboveLayer);
            flipped = true;
            StartCoroutine("FlipCard");
        }
    }
    public void OnDrag(PointerEventData eventData)
    {
        if (!matched && !isPicture)
        {
            this.transform.position = eventData.position;
            //this.GetComponent<Graphic>().color = Color.blue;
        }
    }
    public void OnEndDrag(PointerEventData eventData)
    {
        this.GetComponent<Graphic>().color = Color.white;
        dragged = false;
        if (!matched && !isPicture)
        {
            transform.SetParent(originalParent);
        }
        //Debug.Log(dragged);
    }

    private void Update()
    {
        if (!matched && !dragged && !returning && !home && !isPicture) //player lets go of card
        {
            if (otherCard != null && otherCard.isPicture) //Touch picture card
            {
                //Right Card
                if (otherCard.cardName == cardName)
                {
                    Match();
                    SetFeedbackText(true);
                }
                //Wrong Card
                else
                {
                    SetFeedbackText(false);
                    StartCoroutine("ReturnToOriginalPosition");
                }
            }
            else
            {
                StartCoroutine("ReturnToOriginalPosition");
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Debug.Log("triggered");
        otherCard = collision.gameObject.GetComponent<Card>();
        if (otherCard.isPicture && !returning)
        {
            otherCard.GetComponent<Graphic>().color = Color.grey;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (otherCard != null)
        {
            if (otherCard.isPicture)
            {
                otherCard.GetComponent<Graphic>().color = Color.white;
            }
        }
        GetComponent<Graphic>().color = Color.white;
        otherCard = null;
    }

    void Match()
    {
        if (!matched)
        {
            Debug.Log("Matched!");
            if (MatchedCard != null)
                MatchedCard(); //Match Card Event!
            StartCoroutine(MoveToMatch(otherCard.transform));
            SpawnExplosion();
            matched = true;
        }
    }

    void SpawnExplosion()
    {
        GameObject explosion = Instantiate(explosionPrefab, transform.parent);
        Destroy(explosion, 0.5f);
    }

    IEnumerator MoveToMatch(Transform position)
    {
        while (Vector3.Distance(transform.position, position.position) > 0.1f)
        {
            transform.position = Vector3.Lerp(transform.position, position.position, Time.deltaTime * smoothing);
            yield return null;   
        }
    }

    IEnumerator ReturnToOriginalPosition()
    {
        while (Vector3.Distance(transform.position, originalPosition) > 0.5f)
        {
            transform.position = Vector3.Lerp(transform.position, originalPosition, Time.deltaTime * smoothing);
            returning = true;
            yield return null;
        }
        Debug.Log("Returned to original Position");
        returning = false;
        home = true;
        flipped = false;
        StartCoroutine("FlipCard");
    }

    IEnumerator FlipCard()
    {
        bool done = false;

        while (!done)
        {
            transform.Rotate(new Vector3(0, 10f, 0));
            if (transform.eulerAngles.y > 90f)
            {
                transform.localScale = new Vector3(-1, 1, 1);
                if (flipped)
                {
                    sprite.sprite = face;
                }
                else if (!flipped)
                {
                    sprite.sprite = back;
                }
            }
            if (transform.eulerAngles.y > 180f )
            {
                done = true;
            }
            yield return null;
        }
        transform.localScale = new Vector3(1, 1, 1);
        transform.eulerAngles = Vector3.zero;
    }

    void SetFeedbackText(bool isCorrect)
    {
        if (isCorrect)
        {
            feedbackText.color = new Color(0, 0.69f, 0.117f);
            feedbackText.text = "Correct!";
        }
        else if (!isCorrect)
        {
            feedbackText.color = Color.red;
            feedbackText.text = "Wrong!";
        }
        Invoke("ResetFeedbackText", 1.0f);
    }

    private void ResetFeedbackText()
    {
        feedbackText.text = "";
    }
}
